if [ ! $# == 2 ]; then
  echo -n "
copyFile PATH N

PATH - path to file
       if there is number it will be incremented
       otherwise will start with 1

N - number of copies to do"
  exit 1
fi

NAME_PATTERN=([1-9][[:digit:]]*)$
FILE_PATH=$1
COUNT=$2

if [[ $FILE_PATH =~ $NAME_PATTERN ]]; then
  NUMBER=${BASH_REMATCH[1]}
  NUMBER_LEN=$(echo -n $NUMBER | wc -m)
  PREFIX=${FILE_PATH::((-$NUMBER_LEN))}
else
  NUMBER=0
  PREFIX=$FILE_PATH
fi

for ((i = 0; i < $COUNT; i++)); do
  NUMBER=$(($NUMBER + 1))
  COPY_NAME=$PREFIX$NUMBER

  cp $FILE_PATH $COPY_NAME
done
