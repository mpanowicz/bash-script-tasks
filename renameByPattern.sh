if [ ! $# -ge 3 ]; then
  echo -n "
rename PREFIX SOURCE DESTINATION REPLACE

PREFIX - pattern to remove
SOURCE - folder with files to move
DESTINATION - folder for files after rename
REPLACE - replace prefix with given text"
  exit 1
elif [ -z  "$1" ]; then
  echo "Prefix can not be empty"
  exit 1
elif [ ! -d "$2" ]; then
  echo "$2 not exists"
  exit 1
elif [ ! -d "$3" ]; then
  echo "$3 not exists"
  exit 1
fi

PREFIX=$1
SOURCE=$2
DESTINATION=$3
REPLACE=$4

for FILE in "$SOURCE"/*; do
  if [[ $FILE =~ ^$SOURCE/$PREFIX(.*)$ ]]; then
    NAME=${BASH_REMATCH[1]}
    mv $FILE "$DESTINATION/$REPLACE$NAME"
  fi
done
