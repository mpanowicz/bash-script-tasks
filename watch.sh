trap ctrl_c INT

function ctrl_c() {
  echo "Watch ended"
  exit 0
}

if [ $# == 0 ]; then
  echo -n "
watch COMMAND
watch INRERVAL COMMAND

COMMAND - string command to call
INTERVAL - how often refresh data, default 1s"
  exit
elif [ $# == 1 ]; then
  COMMAND=$1
  INTERVAL=1
else
  INTERVAL=$1
  COMMAND=$2
fi

while [ 1 ]; do
  clear
  tput home
  echo $COMMAND | bash
  sleep $INTERVAL
done
